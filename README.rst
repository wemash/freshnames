Freshnames
==========

When using `Django`_, uploaded files don't get their name changed, which means
that a file upload may overwrite an old one if they have the same name. Instead
of worrying about that, why not just ensure that every uploaded file gets a
fresh name?

Freshnames will not lose track of your file's extension, should that matter to
you. You can also place the file anywhere by giving :code:`freshname` a series
of prefixed directories. Don't worry about joining them yourself -
:code:`freshname`'s got that taken care of.

You can use :code:`freshname` even if you aren't using `Django`_::

  from freshnames import freshname
  freshname("SOME_FILENAME.EXT")

Use :code:`freshgen` if you want to create a filename generator to use with an
:code:`ImageField`::

  from freshnames import freshgen
  my_image = models.ImageField(upload_to = freshgen())

You can also pass multiple prefix paths to either :code:`freshname` or
:code:`freshgen`; they'll be joined for you using your platform's default path
delimiter::

  from freshnames import freshgen
  freshname("FILE.EXT", "FIRST", "SECOND", "THIRD")
  my_image = models.ImageField(upload_to = freshgen("FIRST", "SECOND", "THIRD"))

.. _Django: https://www.djangoproject.com
