from freshnames import freshhash, freshname, freshgen
from re import search

def matches(thing, pattern):
    """Determine if a thing matches a pattern"""
    return search(pattern, thing) is not None

def test_generates_fresh_hashes():
    assert freshhash() != freshhash()

def test_generates_fresh_names():
    assert matches(freshname("ASDF.jpg"), r"\.jpg$")

def test_generates_fresh_names_with_prefixes():
    assert matches(freshname("ASDF.jpg", "QWERTY"), r"^QWERTY.*?\.jpg$")
    assert matches(freshname("ASDF.jpg", "QWERTY", "HJKL"), r"^QWERTY/HJKL.*?\.jpg$")

def test_generates_fresh_generators():
    generator = freshgen()
    assert matches(generator(None, "ASDF.jpg"), r"\.jpg$")

def test_doesnt_add_an_extra_dot():
    assert not matches(freshname("ASDF.jpg"), r"\.\.jpg$")

def test_generator_can_deconstruct_self():
    assert freshgen().deconstruct() == ("freshnames.FreshGenerator", tuple(), {})

def test_generator_returns_construction_args_when_deconstructing():
    assert freshgen("ASDF", "HJKL").deconstruct()[1] == ("ASDF", "HJKL")
    
