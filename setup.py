from distutils.core import setup

setup(
    name = "freshnames",
    version = "1.0.3",
    description = "No more file upload overwrites!",
    author = "Kenneth Pullen",
    author_email = "ken@mash.is",
    url = "https://bitbucket.org/wemash/freshnames",
    py_modules = ["freshnames"]
)

