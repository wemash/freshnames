"""
Freshnames
==========

When using `Django`_, uploaded files don't get their name changed, which means
that a file upload may overwrite an old one if they have the same name. Instead
of worrying about that, why not just ensure that every uploaded file gets a
fresh name?

Freshnames will not lose track of your file's extension, should that matter to
you. You can also place the file anywhere by giving :code:`freshname` a series
of prefixed directories. Don't worry about joining them yourself -
:code:`freshname`'s got that taken care of.

You can use :code:`freshname` even if you aren't using `Django`_::

  from freshnames import freshname
  freshname("SOME_FILENAME.EXT")

Use :code:`freshgen` if you want to create a filename generator to use with an
:code:`ImageField`::

  from freshnames import freshgen
  my_image = models.ImageField(upload_to = freshgen())

You can also pass multiple prefix paths to either :code:`freshname` or
:code:`freshgen`; they'll be joined for you using your platform's default path
delimiter::

  from freshnames import freshgen
  freshname("FILE.EXT", "FIRST", "SECOND", "THIRD")
  my_image = models.ImageField(upload_to = freshgen("FIRST", "SECOND", "THIRD"))

.. _Django: https://www.djangoproject.com
"""

def freshhash():
    """Generate a new UUID as a string"""
    import uuid
    return str(uuid.uuid1())

def freshname(filename, *prefixes):
    """
    Generate a fresh file name

    `freshname` understands that sometimes file extensions are important, and
    will do its best to preserve your filename's extension.

    @param filename: The name of the file you want to generate a name for
    @param prefixes: A list of path chunks you want prefixed to the filenames
    this generator generates
    """
    from os.path import splitext, join
    _, ext = splitext(filename)
    chunks = list(prefixes)
    chunks.append("")
    chunks.append("%s%s" % (freshhash(), ext))
    return join(*chunks)

class FreshGenerator:
    """A callable generator of fresh names"""
    def __init__(self, *prefixes):
        self.prefixes = prefixes

    def __call__(self, instance, filename):
        return freshname(filename, *self.prefixes)

    def deconstruct(self):
        return ("freshnames.FreshGenerator", self.prefixes, {})

def freshgen(*prefixes):
    """
    Create a new freshname generator.

    Generators generate fresh filenames when given a filename. They've been
    designed to specifically work with `Django`_. You can still use a generator
    on your own, but it's not recommended. Use `freshname` instead.

    @param prefixes: A list of path chunks you want prefixed to the filenames
    this generator generates
    """
    return FreshGenerator(*prefixes)
